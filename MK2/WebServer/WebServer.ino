#include <FS.h>                   //this needs to be first, or it all crashes and burns...
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>

ESP8266WebServer ObjGlobal_HttpServer(80);

void setup()
{
  Serial.begin(115200);
  Serial.println();

  Serial.print("Setting soft-AP ... ");
  boolean result = WiFi.softAP("RF_WebServer", "123456789");

  if(result == true){
    Serial.println("Ready");
  }
  else{
    Serial.println("Failed!");
  }
  
  SetupWebHandlers();

}

void loop(){

  ObjGlobal_HttpServer.handleClient();
}

void HandleWebRoot(){
  ObjGlobal_HttpServer.sendHeader("Location", "/index.html",true);   //Redirect to our html web page
  ObjGlobal_HttpServer.send(302, "text/plane","");
  Serial.println("Handled");
}

void handleWebRequests(){  
  if(loadFromSpiffs(ObjGlobal_HttpServer.uri())) return;
  String message = "File Not Detected\n\n";
  message += "URI: ";
  message += ObjGlobal_HttpServer.uri();
  message += "\nMethod: ";
  message += (ObjGlobal_HttpServer.method() == HTTP_GET)?"GET":"POST";
  message += "\nArguments: ";
  message += ObjGlobal_HttpServer.args();
  message += "\n";
  for (uint8_t i=0; i<ObjGlobal_HttpServer.args(); i++){
    message += " NAME:"+ObjGlobal_HttpServer.argName(i) + "\n VALUE:" + ObjGlobal_HttpServer.arg(i) + "\n";
  }
  ObjGlobal_HttpServer.send(404, "text/plain", message);
  Serial.println(message);
}

bool loadFromSpiffs(String path){

  String dataType = "text/plain";
  if(path.endsWith("/")) path += "index.htm";

  if(path.endsWith(".src")) path = path.substring(0, path.lastIndexOf("."));
  else if(path.endsWith(".html")) dataType = "text/html";
  else if(path.endsWith(".htm")) dataType = "text/html";
  else if(path.endsWith(".css")) dataType = "text/css";
  else if(path.endsWith(".js")) dataType = "application/javascript";
  else if(path.endsWith(".png")) dataType = "image/png";
  else if(path.endsWith(".gif")) dataType = "image/gif";
  else if(path.endsWith(".jpg")) dataType = "image/jpeg";
  else if(path.endsWith(".ico")) dataType = "image/x-icon";
  else if(path.endsWith(".xml")) dataType = "text/xml";
  else if(path.endsWith(".pdf")) dataType = "application/pdf";
  else if(path.endsWith(".zip")) dataType = "application/zip";

	SPIFFS.begin();

  File dataFile = SPIFFS.open(path.c_str(), "r");
  
  if (ObjGlobal_HttpServer.hasArg("download")) dataType = "application/octet-stream";
  if (ObjGlobal_HttpServer.streamFile(dataFile, dataType) != dataFile.size()) {
  }
  dataFile.close();
  
  SPIFFS.end();

  return true;
}

void SetupWebHandlers(){
  // setup root page
  
  ObjGlobal_HttpServer.on("/", [](){
    // if(!ObjGlobal_HttpServer.authenticate(SettingsData.update_username, SettingsData.update_password))
    //   return ObjGlobal_HttpServer.requestAuthentication();
    
    
    ObjGlobal_HttpServer.send(200, "text/html", "<h1>You are connected</h1>");
    //HandleWebRoot();
    Serial.println("i'm here");
  });

  // // reset, reboot, wipe
  // ObjGlobal_HttpServer.on("/osSystem", [](){
  //   if(!ObjGlobal_HttpServer.authenticate(SettingsData.update_username, SettingsData.update_password))
  //     return ObjGlobal_HttpServer.requestAuthentication();
    
  //   HandleOsSystem();

  // });

  // ObjGlobal_HttpServer.on("/osAction", [](){
  //   if(!ObjGlobal_HttpServer.authenticate(SettingsData.update_username, SettingsData.update_password))
  //     return ObjGlobal_HttpServer.requestAuthentication();
    
  //   HandleOsAction();

  // });

  // ObjGlobal_HttpServer.on("/osStatus", [](){
  //   if(!ObjGlobal_HttpServer.authenticate(SettingsData.update_username, SettingsData.update_password))
  //     return ObjGlobal_HttpServer.requestAuthentication();
    
  //   HandleOsStatus();

  // });


  // // setup reset page
  // ObjGlobal_HttpServer.on("/osConfig", [](){
  //   if(!ObjGlobal_HttpServer.authenticate(SettingsData.update_username, SettingsData.update_password))
  //     return ObjGlobal_HttpServer.requestAuthentication();
    
  //   HandleOsConfig();

  // });

  ObjGlobal_HttpServer.onNotFound(handleWebRequests);

  ObjGlobal_HttpServer.begin();
  
}
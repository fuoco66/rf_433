/*
  Example for different sending methods
  
  https://github.com/sui77/rc-switch/
  
*/

#include <RCSwitch.h>

// #define BUTTON_PIN 9 // arduino
#define BUTTON_PIN 12 // esp8266
#define STATUS_LED_1 LED_BUILTIN

RCSwitch mySwitch = RCSwitch();

void setup() {

  Serial.begin(115200);
  
  // mySwitch.enableTransmit(10); // Arduino Pin #10 
  mySwitch.enableTransmit(14); // Esp8266 Pin #10 
  pinMode(STATUS_LED_1, OUTPUT);
  pinMode(BUTTON_PIN, INPUT_PULLUP);
  
  // Optional set protocol (default is 1, will work for most outlets)
  // mySwitch.setProtocol(2);

  // Optional set pulse length.
  mySwitch.setPulseLength(312);
  
  // Optional set number of transmission repetitions.
  // mySwitch.setRepeatTransmit(15);
  
}

void loop() {

  int buttonState = !digitalRead(BUTTON_PIN);

  if(buttonState){
    digitalWrite(STATUS_LED_1, HIGH);
    
    Serial.println("Send Integer");
    mySwitch.send(3696136, 24); 
    delay(10);
  }
  else {
    digitalWrite(STATUS_LED_1, LOW);
  }


}


// Decimal: 3696136 (24Bit) Binary: 001110000110011000001000 Tri-State: not applicable PulseLength: 319 microseconds Protocol: 1
// Raw data: 9896,324,964,324,972,980,312,980,316,976,320,312,980,312,980,316,976,316,976,976,316,976,320,308,980,312,980,968,324,968,328,300,988,308,984,308,984,308,984,308,980,976,320,308,984,308,984,312,980,

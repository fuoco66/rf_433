/*
  Example for receiving
  
  https://github.com/sui77/rc-switch/
  
  If you want to visualize a telegram copy the raw data and 
  paste it into http://test.sui.li/oszi/
*/

#include <RCSwitch.h>

RCSwitch mySwitch = RCSwitch();

void setup() {
  Serial.begin(115200);
  // mySwitch.enableReceive(0);  // Receiver on interrupt 0 => that is pin #2 for Arduino
  mySwitch.enableReceive(4);  // For ESP8266 use D2 (GPIO4), Rc switch uses the GPIO naming scheme. 3.3V is fine
  Serial.println("Reciever Ready");
}

void loop() {
  if (mySwitch.available()) {
    output(mySwitch.getReceivedValue(), mySwitch.getReceivedBitlength(), mySwitch.getReceivedDelay(), mySwitch.getReceivedRawdata(),mySwitch.getReceivedProtocol());
    mySwitch.resetAvailable();
  }
}

void StartRx(){
  
  // Setup Reciever
  RxDevice.enableReceive(RX_PIN);
  RxDevice.resetAvailable();

  StatusData.collectedMsg = 0;
  StatusData.rxCaptureSession++;
  StatusData.machineState = STATE_RX_ON;

}

void StopRx(){

  // clean recieved message (of the library)  
  RxDevice.resetAvailable();

  // Setup Reciever
  RxDevice.disableReceive();
  StartReadyState();

}

void CheckRx(){
  
  if(StatusData.machineState != STATE_RX_ON &&
     StatusData.machineState != STATE_RX_IN){
    // rx turned off
    return;
  }

  if (RxDevice.available()) {

    StatusData.machineState = STATE_RX_IN;

    unsigned long decimal = RxDevice.getReceivedValue();
    unsigned int length = RxDevice.getReceivedBitlength();

    RxMessage CurrentMsg = (RxMessage){ millis(),
                                        decimal,
                                        dec2binWzerofill(decimal, length),
                                        // RxDevice.getReceivedRawdata(),
                                        length,
                                        RxDevice.getReceivedDelay(),
                                        RxDevice.getReceivedProtocol()};
    

    RxDevice.resetAvailable();
    StoreMsg(CurrentMsg);

  }

}

void StoreMsg(RxMessage Msg){

  // clear array on new capture
  if(StatusData.collectedMsg > RX_MAX_MSG_COUNT){
    // StatusData.RxMessages = [];
    StatusData.collectedMsg = 0;
  }

  // if first mgs, store without controls
  if(StatusData.collectedMsg == 0){
    StatusData.RxMessages[StatusData.collectedMsg] = Msg;
    StatusData.collectedMsg++;
    return;
  }

  // check if current msg code is the same as previous
  if(Msg.decimal == StatusData.RxMessages[StatusData.collectedMsg-1].decimal){

    // store, and increment
    StatusData.RxMessages[StatusData.collectedMsg] = Msg;
    StatusData.collectedMsg++;
  }

    // capture goal reached
  if(StatusData.collectedMsg == RX_MAX_MSG_COUNT){
    
    Sprint("captured messages: ");
    Sprintln(StatusData.collectedMsg);
    StartReadyState();

  }

}

static char * dec2binWzerofill(unsigned long Dec, unsigned int bitLength) {
  static char bin[64]; 
  unsigned int i=0;

  while (Dec > 0) {
    bin[32+i++] = ((Dec & 1) > 0) ? '1' : '0';
    Dec = Dec >> 1;
  }

  for (unsigned int j = 0; j< bitLength; j++) {
    if (j >= bitLength - i) {
      bin[j] = bin[ 31 + i - (j - (bitLength - i)) ];
    } else {
      bin[j] = '0';
    }
  }
  bin[bitLength] = '\0';
  
  return bin;
}

#if DEBUG
void PrintTest(RxMessage Msg){
 
  Sprint("Decimal: ");
  Sprint(Msg.decimal);
  Sprint(" (");
  Sprint( Msg.length );
  Sprint("Bit) Binary: ");
  Sprint( Msg.binary );
  // Sprint(" Tri-State: ");
  // Sprint( bin2tristate( b) );
  Sprint(" PulseLength: ");
  Sprint(Msg.pulseLength);
  Sprint(" microseconds");
  Sprint(" Protocol: ");
  Sprintln(Msg.protocol);
  
  // Serial.print("Raw data: ");
  // for (unsigned int i=0; i<= length*2; i++) {
  //   Serial.print(raw[i]);
  //   Serial.print(",");
  // }
  Sprintln();
  Sprintln();
}
#endif

void StartTx(){

  TxDevice.enableTransmit(TX_PIN);
  
  TxDevice.setProtocol(StatusData.TxMsg.protocol);
  TxDevice.setPulseLength(StatusData.TxMsg.pulseLength);
  TxDevice.setRepeatTransmit(StatusData.TxMsg.repeat);

}

void StopTx(){
  readyForTx = false;
  TxDevice.disableTransmit();
  StartReadyState();
}

void CheckTx(){

  // Sends out transmission only if message ready and status updated    
  if(StatusData.machineState != STATE_TX_OUT || readyForTx == false){
    return;
  }

  TxDevice.send(StatusData.TxMsg.code, StatusData.TxMsg.length); 
}

void PrepareTx(DynamicJsonDocument StatusPayload){

  StatusData.TxMsg.code = StatusPayload["msg"]["code"];
  StatusData.TxMsg.pulseLength = StatusPayload["msg"]["pulseLength"];
  StatusData.TxMsg.protocol = StatusPayload["msg"]["protocol"];
  StatusData.TxMsg.repeat = StatusPayload["msg"]["repeat"];
  StatusData.TxMsg.length = StatusPayload["msg"]["length"];    
  
  StartTx();

  // set to on, the status manager will set to out
  StatusData.machineState = STATE_TX_ON;

  // Sprint("code: ");
  // Sprintln(StatusData.TxMsg.code);
  // Sprint("pulseLength: ");
  // Sprintln(StatusData.TxMsg.pulseLength);
  // Sprint("protocol: ");
  // Sprintln(StatusData.TxMsg.protocol);
  // Sprint("repeat: ");
  // Sprintln(StatusData.TxMsg.repeat);
  // Sprint("length: ");
  // Sprintln(StatusData.TxMsg.length);

}

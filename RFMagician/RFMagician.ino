#include "Hardware.h"

const char* configfile = "/config.json";

ESP8266WebServer ObjGlobal_HttpServer(80);
ESP8266HTTPUpdateServer HttpUpdater;

/* Create DNS server instance to enable captive portal. */
DNSServer DnsServer;
IPAddress apIP(192, 168, 4, 1);

RCSwitch RxDevice = RCSwitch();
RCSwitch TxDevice = RCSwitch();

RxMessage Global_RxMsgArray[RX_MAX_MSG_COUNT];
uint8_t Global_RxCollectedMsg = 0;
  
TimedAction StatusOutputThread = TimedAction(50, StatusOutput);
// TimedAction TxThread = TimedAction(40, StatusOutput);
bool readyForTx = false;

void setup() {

  Sbegin(115200);

  SetupWifiAp();
  SetupWebHandlers();

  // redirects everything to the local ip address
  DnsServer.setErrorReplyCode(DNSReplyCode::NoError);
  DnsServer.start(53, "*", apIP);

  pinMode(12, INPUT_PULLUP);
}

void loop() {
  

  // update status based on ajax requests
  StatusOutputThread.check();

  // if no client connected no action is performed
  if(!isClientConnected()){
    
    if(StatusData.machineState != STATE_IDLE){
      StartIdleState();
    }

    return;
  }
  else if(StatusData.machineState == STATE_IDLE){
    StartReadyState();
    Sprintln("loop ready state");
  }

  // manage dns requests
  // DnsServer.processNextRequest();
  
  ObjGlobal_HttpServer.handleClient();

  // if rx is enabled, check if there are new messages
  CheckRx();

  // if tx is in out state, sends the code out
  CheckTx();

}

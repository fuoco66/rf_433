bool CreateConfigFile(){
	
	DynamicJsonDocument jsonDoc(1024);

	// load default data from struct init	
	// jsonDoc["autoWater"] = ConfigurationData.autoWater;
	// jsonDoc["pumpPin"] = ConfigurationData.pumpPin;
	// jsonDoc["pumpPinInverted"] = ConfigurationData.pumpPinInverted;
	
	// jsonDoc["waterLevelPin"] = ConfigurationData.waterLevelPin;
	// jsonDoc["waterLevelPinInverted"] = ConfigurationData.waterLevelPinInverted;

	// jsonDoc["waterTime"] = ConfigurationData.waterTime;

	// jsonDoc["startHour"] = ConfigurationData.startHour;
	// jsonDoc["startMinutes"] = ConfigurationData.startMinutes;
	// jsonDoc["stopHour"] = ConfigurationData.stopHour;
	// jsonDoc["stopMinutes"] = ConfigurationData.stopMinutes;

	File ConfigFile = SPIFFS.open(configfile, "w");
	if (!ConfigFile) {
		Sprintln("Failed to create file");
		return false;
	}

	serializeJson(jsonDoc, Serial);
	serializeJson(jsonDoc, ConfigFile);

	ConfigFile.close();
	Sprintln("File created");

	return true;
}

// Load the buttons config from file
void LoadConfig(){

	if (!SPIFFS.begin()) {
		Sprintln("Config SPIFF FAILED");
		return;
	}
	
	Sprintln("Config Spiff begin");

	if(!SPIFFS.exists(configfile)) {
		Sprintln("File NOT found, creating");
		if(CreateConfigFile() != true){
			return;
		}
	}
		
	Sprintln("Config File found");
	
	//file exists, reading and loading
	File objConfigFile = SPIFFS.open(configfile, "r");

	if(!objConfigFile){
		Sprintln("Failed to open Button file");
		return;
	}

	// Sprintln("Button File opened");
	// for debug, print the whole file
	// while (objConfigFile.available()) {

	//     Serial.write(objConfigFile.read());
	// }

	size_t size = objConfigFile.size();
	// Allocate a buffer to store contents of the file.
	std::unique_ptr<char[]> buf(new char[size]);

	objConfigFile.readBytes(buf.get(), size);

	DynamicJsonDocument jsonBuffer(1024);
	DeserializationError jsonError = deserializeJson(jsonBuffer, buf.get());
	
	if (jsonError){
		Sprintln("Failed to parse config json: ");
		switch (jsonError.code()) {
			case DeserializationError::InvalidInput:
					Sprintln(F("Invalid input!"));
					break;
			case DeserializationError::NoMemory:
					Sprintln(F("Not enough memory"));
					break;
			default:
					Sprintln(F("Deserialization failed"));
					break;
		}

		objConfigFile.close();
		return;
	}

	Sprintln("config File Loaded");

	// ConfigurationData.autoWater = jsonBuffer["autoWater"].as<bool>();
	// ConfigurationData.pumpPin = jsonBuffer["pumpPin"];
	// ConfigurationData.pumpPinInverted = jsonBuffer["pumpPinInverted"].as<bool>();
	
	// ConfigurationData.waterLevelPin = jsonBuffer["waterLevelPin"];
	// ConfigurationData.waterLevelPinInverted = jsonBuffer["waterLevelPinInverted"].as<bool>();

	// ConfigurationData.waterTime = jsonBuffer["waterTime"];

	// ConfigurationData.startHour = jsonBuffer["startHour"];
	// ConfigurationData.startMinutes = jsonBuffer["startMinutes"];
	// ConfigurationData.stopHour = jsonBuffer["stopHour"];
	// ConfigurationData.stopMinutes = jsonBuffer["stopMinutes"];

}

// load the config from memory
DynamicJsonDocument WebGetConfig(){

	DynamicJsonDocument jsonDoc(1024);
	JsonObject root = jsonDoc.to<JsonObject>();

	// root["autoWater"] = ConfigurationData.autoWater;
	// root["pumpPin"] = ConfigurationData.pumpPin;
	// root["pumpPinInverted"] = ConfigurationData.pumpPinInverted;

	// root["waterLevelPin"] = ConfigurationData.waterLevelPin;
	// root["waterLevelPinInverted"] = ConfigurationData.waterLevelPinInverted;

	// root["waterTime"] = ConfigurationData.waterTime;

	// root["startHour"] = ConfigurationData.startHour;
	// root["startMinutes"] = ConfigurationData.startMinutes;
	// root["stopHour"] = ConfigurationData.stopHour;
	// root["stopMinutes"] = ConfigurationData.stopMinutes;
	
	return jsonDoc;

}

void SaveConfigFile(String payLoad, String strFileName){

	DynamicJsonDocument jsonBuffer(2048);
	DeserializationError jsonError = deserializeJson(jsonBuffer, payLoad);

	if (jsonError){
		Sprintln("Failed to parse json");
		return;
	}

	File objFile = SPIFFS.open(strFileName, "w");
	if (!objFile) {
		Sprintln("Failed to open file");
		return;
	}

	Sprintln("Writing file");
	// serializeJson(jsonBuffer["value"], Serial);

	serializeJson(jsonBuffer["value"], objFile);
	Sprintln("File Written");

	objFile.close();
		
	Sprintln("Writing END");
	
}

// void WifiReset(){
// 	ObjGlobal_WifiManager.resetSettings();
// 	ESP.reset();
// }

void FullReset(){

	if (SPIFFS.begin()) {

		if (SPIFFS.exists(configfile)) {
			SPIFFS.remove(configfile);
		}

		SPIFFS.end();

	}
}

void Reboot(){
	ESP.reset();
}

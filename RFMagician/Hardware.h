// "compilerPath": "/opt/arduino-1.8.13/hardware/tools/avr/bin/avr-gcc", "/usr/bin/cpp",

// "/home/manu/.arduino15/packages/esp8266/tools/xtensa-lx106-elf-gcc/2.5.0-4-b40a506/xtensa-lx106-elf/include/c++/4.8.2/",
// "/home/manu/.arduino15/packages/esp8266/tools/xtensa-lx106-elf-gcc/2.5.0-4-b40a506/xtensa-lx106-elf/include/sys/",
// "/home/manu/.arduino15/packages/esp8266/tools/xtensa-lx106-elf-gcc/2.5.0-4-b40a506/xtensa-lx106-elf/include/c++/4.8.2/xtensa-lx106-elf/bits/"
#include <FS.h>                   //this needs to be first, or it all crashes and burns...
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPUpdateServer.h>
#include <DNSServer.h>
#include <ArduinoJson.h>          //https://github.com/bblanchon/ArduinoJson
#include <TimedAction.h>
#include <RCSwitch.h>

#define STATUS_LED_1 2

#define AP_NAME "RFMagician"
#define AP_PSWD "123456789"
#define AP_CH 1
#define AP_HIDDEN false
#define AP_MAX_CONN 2

#define UI_USER "manu" 
#define UI_PSWD "1234"
#define UI_UPD_PATH "/firmware"

// For ESP8266 use D2 (GPIO4), Rc switch uses the GPIO naming scheme. 3.3V is fine
#define RX_PIN 4 // ESP8266 
// #define RX_PIN 0 // ARDUINO

#define TX_PIN 14 // ESP8266
// #define TX_PIN 10 // Arduino

#define RX_MAX_MSG_COUNT 25

#define STATE_IDLE 0
#define STATE_READY 1

#define STATE_RX_ON 2
#define STATE_RX_IN 3

#define STATE_TX_ON 4
#define STATE_TX_OUT 5

#define STATE_FATAL_ERROR 99    


typedef struct RxMessage{
  unsigned long timeStamp;
  unsigned long decimal;
  char* binary;
  // unsigned int* raw;
  unsigned int length;
  unsigned int pulseLength;
  unsigned int protocol;
} RxMessage;

typedef struct TxMessage {
  unsigned long code;
  unsigned int pulseLength;
  unsigned int protocol;
  unsigned int repeat;
  unsigned int length;  
} TxMessage;

struct Status {
  uint8_t machineState;
  uint8_t lastMachineState;
  uint8_t collectedMsg;
  uint8_t rxCaptureSession;
  RxMessage RxMessages[RX_MAX_MSG_COUNT];
  TxMessage TxMsg;
} StatusData = {
  STATE_IDLE,
  STATE_FATAL_ERROR,
  0,
  0,
  {},
  {}
};


void StatusOutput();

#define DEBUG 1

#if DEBUG
  #define Sbegin(x) (Serial.begin(x))
  #define Sprintln(x) (Serial.println(x))
  #define Sprint(x) (Serial.print(x))
  #define Sprintf(x,y) (Serial.print(x))
#else
  #define Sbegin(x)
  #define Sprintln(x)
  #define Sprint(x)
#endif

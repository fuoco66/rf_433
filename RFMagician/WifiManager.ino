bool SetupWifiAp(){

  WiFi.mode(WIFI_AP);
  WiFi.softAPConfig(apIP, apIP, IPAddress(255, 255, 255, 0));

  boolean result = WiFi.softAP(AP_NAME, AP_PSWD, AP_CH, AP_HIDDEN, AP_MAX_CONN);
  if(result == false){
    Sprintln("Access Point creation failed");

    StatusData.machineState = STATE_FATAL_ERROR;
    
    return false;
  }

  Sprintln("Access Point created!");
  Sprint("AP Name: ");
  Sprintln(AP_NAME);
  Sprint("AP pswd: ");
  Sprintln(AP_PSWD);

  // // Setup web updater
  HttpUpdater.setup(&ObjGlobal_HttpServer, UI_UPD_PATH, UI_USER, UI_PSWD);

  return true;
}

void HandleWebRoot(){
  ObjGlobal_HttpServer.sendHeader("Location", "/index.html",true);   //Redirect to our html web page
  ObjGlobal_HttpServer.send(302, "text/plane","");
}

void handleWebRequests(){  
  if(loadFromSpiffs(ObjGlobal_HttpServer.uri())){
    return;
  }
  
  String responseHTML = ""
                      "<!DOCTYPE html><html lang='en'><head>"
                      "<meta name='viewport' content='width=device-width'>"
                      "<title>CaptivePortal</title></head><body>"
                      "<h1>Hello World!</h1><p>This is a captive portal example."
                      " All requests will be redirected here.</p></body></html>";
  ObjGlobal_HttpServer.send(200, "text/html", responseHTML);
  return;

  String message = "File Not Detected\n\n";
  message += "URI: ";
  message += ObjGlobal_HttpServer.uri();
  message += "\nMethod: ";
  message += (ObjGlobal_HttpServer.method() == HTTP_GET)?"GET":"POST";
  message += "\nArguments: ";
  message += ObjGlobal_HttpServer.args();
  message += "\n";
  for (uint8_t i=0; i<ObjGlobal_HttpServer.args(); i++){
    message += " NAME:"+ObjGlobal_HttpServer.argName(i) + "\n VALUE:" + ObjGlobal_HttpServer.arg(i) + "\n";
  }
  ObjGlobal_HttpServer.send(404, "text/plain", message);
  Serial.println(message);
}

bool loadFromSpiffs(String path){

  String dataType = "text/plain";
  if(path.endsWith("/")) path += "index.htm";

  if(path.endsWith(".src")) path = path.substring(0, path.lastIndexOf("."));
  else if(path.endsWith(".html")) dataType = "text/html";
  else if(path.endsWith(".htm")) dataType = "text/html";
  else if(path.endsWith(".css")) dataType = "text/css";
  else if(path.endsWith(".js")) dataType = "application/javascript";
  else if(path.endsWith(".png")) dataType = "image/png";
  else if(path.endsWith(".gif")) dataType = "image/gif";
  else if(path.endsWith(".jpg")) dataType = "image/jpeg";
  else if(path.endsWith(".ico")) dataType = "image/x-icon";
  else if(path.endsWith(".xml")) dataType = "text/xml";
  else if(path.endsWith(".pdf")) dataType = "application/pdf";
  else if(path.endsWith(".zip")) dataType = "application/zip";

	SPIFFS.begin();

  File dataFile = SPIFFS.open(path.c_str(), "r");
  
  if (ObjGlobal_HttpServer.hasArg("download")) dataType = "application/octet-stream";
  if (ObjGlobal_HttpServer.streamFile(dataFile, dataType) != dataFile.size()) {
  }
  dataFile.close();
  
  SPIFFS.end();

  return true;
}

bool isClientConnected(){
  
  uint8_t connectedClient = WiFi.softAPgetStationNum();

  if(connectedClient >= 1){
    return true;
  }

  return false;
}

void SetupWebHandlers(){

  // setup root page

  // captive test  
  ObjGlobal_HttpServer.on("/generate_204", HandleWebRoot ); // android captive portal
  ObjGlobal_HttpServer.on("/fwlink", HandleWebRoot ); // windows captive portal

  ObjGlobal_HttpServer.on("/", [](){
    // if(!ObjGlobal_HttpServer.authenticate(UI_USER, UI_PSWD))
    //   return ObjGlobal_HttpServer.requestAuthentication();
        
    HandleWebRoot();
    
  });

  // reset, reboot, wipe
  ObjGlobal_HttpServer.on("/osSystem", [](){
    // if(!ObjGlobal_HttpServer.authenticate(SettingsData.update_username, SettingsData.update_password))
    //   return ObjGlobal_HttpServer.requestAuthentication();
    
    HandleOsSystem();

  });

  ObjGlobal_HttpServer.on("/osAction", [](){
    // if(!ObjGlobal_HttpServer.authenticate(SettingsData.update_username, SettingsData.update_password))
    //   return ObjGlobal_HttpServer.requestAuthentication();
    
    HandleOsAction();

  });

  ObjGlobal_HttpServer.on("/osStatus", [](){
    // if(!ObjGlobal_HttpServer.authenticate(SettingsData.update_username, SettingsData.update_password))
    //   return ObjGlobal_HttpServer.requestAuthentication();
    
    HandleOsStatus();

  });

  // ObjGlobal_HttpServer.on("/osConfig", [](){
  //   if(!ObjGlobal_HttpServer.authenticate(SettingsData.update_username, SettingsData.update_password))
  //     return ObjGlobal_HttpServer.requestAuthentication();
    
  //   HandleOsConfig();

  // });

  ObjGlobal_HttpServer.onNotFound(handleWebRequests);

  ObjGlobal_HttpServer.begin();
  
}

void HandleOsSystem(){

  // get payload
  String strPayload = ObjGlobal_HttpServer.arg("plain");

  // check payload
  if(strPayload == "" || strPayload.length() == 0 || strPayload == "\"\"" || strPayload == "null"){
    Sprintln(strPayload);
    ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"-9\", \"response\":\"EMPTY_PAYLOAD\"}");
    return;
  }

  // parse argument
  DynamicJsonDocument objPayload(2024);
  DeserializationError jsonError = deserializeJson(objPayload, strPayload);
			
  if (jsonError){
    Sprintln("failed to load json config_HandleOsConfig");
    Sprintln(jsonError.c_str());
    ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"-10\", \"response\":\"DECODE_FAILED\"}");
    return;
  }
  // release some memory
  objPayload.shrinkToFit();

  // check action
  if(objPayload["action"] == "" || objPayload["action"] == "\"\"" || objPayload["action"] == "null"){
    Sprintln(strPayload);
    ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"-11\", \"response\":\"EMPTY_ACTION\"}");
    return;
  }

  if(objPayload["action"] == "reboot"){

	  ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"0\", \"response\":\"OK\"}");
    Sprintln("Reboot");
    delay(1000);
    Reboot();

  }
  else if(objPayload["action"] == "fullReset"){
    
	  ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"0\", \"response\":\"OK\"}");
    Sprintln("Full Reset");
    delay(1000);
    FullReset();
  }
  else {
    Sprintln(strPayload);
    ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"-8\", \"response\":\"ACTION_NOT_FOUND\"}");
  }
}

void HandleOsStatus(){

  // get payload
  String strPayload = ObjGlobal_HttpServer.arg("plain");

  // check payload
  if(strPayload == "" || strPayload.length() == 0 || strPayload == "\"\"" || strPayload == "null"){
    Sprintln(strPayload);
    ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"-9\", \"response\":\"EMPTY_PAYLOAD\"}");
    return;
  }

  // parse argument
  DynamicJsonDocument objPayload(2024);
  DeserializationError jsonError = deserializeJson(objPayload, strPayload);
			
  if (jsonError){
    Sprintln("failed to load json");
    Sprintln(jsonError.c_str());
    ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"-10\", \"response\":\"DECODE_FAILED\"}");
    return;
  }
  // release some memory
  objPayload.shrinkToFit();

  // check action
  if(objPayload["action"] == "" || objPayload["action"] == "\"\"" || objPayload["action"] == "null"){
    Sprintln(strPayload);
    ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"-11\", \"response\":\"EMPTY_ACTION\"}");
    return;
  }

  if(objPayload["action"] == "GetStatus"){
    // serializeJson(WebGetStatus(), Serial);

    String webResponse;
	  serializeJson(WebGetStatus(), webResponse);
	  // ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"0\", \"response\":\"OK\"}");
	  ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"0\", \"response\":" + webResponse + "}");

  }
  else {
    Sprintln(strPayload);

    ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"-8\", \"response\":\"ACTION_NOT_FOUND\"}");
  }
}

void HandleOsAction(){

  // get payload
  String strPayload = ObjGlobal_HttpServer.arg("plain");

  // check payload
  if(strPayload == "" || strPayload.length() == 0 || strPayload == "\"\"" || strPayload == "null"){
    Sprintln(strPayload);
    ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"-9\", \"response\":\"EMPTY_PAYLOAD\"}");
    return;
  }

  // parse argument
  DynamicJsonDocument objPayload(2024);
  DeserializationError jsonError = deserializeJson(objPayload, strPayload);
			
  if (jsonError){
    Sprintln("failed to load json");
    Sprintln(jsonError.c_str());
    ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"-10\", \"response\":\"DECODE_FAILED\"}");
    return;
  }
  // release some memory
  objPayload.shrinkToFit();

  // check action
  if(objPayload["action"] == "" || objPayload["action"] == "\"\"" || objPayload["action"] == "null"){
    Sprintln(strPayload);
    ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"-11\", \"response\":\"EMPTY_ACTION\"}");
    return;
  }

  if(objPayload["action"] == "startReady"){
    
    StartReadyState();
	  ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"0\", \"response\":\"OK\"}");

  }
  else if(objPayload["action"] == "startRx"){
    StartRx();
	  ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"0\", \"response\":\"OK\"}");

  }
  else if(objPayload["action"] == "stopRx"){
    
    StopRx();
	  ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"0\", \"response\":\"OK\"}");

  } 
  else if(objPayload["action"] == "sendTx"){
    
    PrepareTx(objPayload);
	  ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"0\", \"response\":\"OK\"}");

  }
  else if(objPayload["action"] == "stopTx"){
    
    StopTx();
	  ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"0\", \"response\":\"OK\"}");

  }
  else {
    Sprintln(strPayload);
    ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"-8\", \"response\":\"ACTION_NOT_FOUND\"}");
  }
}






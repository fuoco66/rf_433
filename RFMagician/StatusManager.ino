DynamicJsonDocument WebGetStatus(){

	DynamicJsonDocument jsonDoc(4096);
	JsonObject root = jsonDoc.to<JsonObject>();

  root["state"] = StatusData.machineState;
  root["rxCaptureSession"] = StatusData.rxCaptureSession;

  JsonArray RxMessages = root.createNestedArray("messages");

  if(StatusData.collectedMsg == RX_MAX_MSG_COUNT){
  
    // iterates for every message
    for (int i = 0; i < StatusData.collectedMsg; i++){

      JsonObject RxMsg = RxMessages.createNestedObject();
      
      RxMsg["decimal"] = StatusData.RxMessages[i].decimal;
      RxMsg["binary"] = StatusData.RxMessages[i].binary;
      RxMsg["length"] = StatusData.RxMessages[i].length;
      RxMsg["pulseLength"] = StatusData.RxMessages[i].pulseLength;
      RxMsg["protocol"] = StatusData.RxMessages[i].protocol;

    }
  }

	return jsonDoc;

}

void StartIdleState(){
    
    // Sprintln("inside startIdleState");

    if(StatusData.machineState == STATE_IDLE){
      return;
    }

    StatusData.machineState = STATE_IDLE;
}

void StartReadyState(){
    
    if(StatusData.machineState == STATE_READY){
      return;
    }

    StatusData.machineState = STATE_READY;
}


void StatusOutput(){

  // no status change
  if(StatusData.machineState == StatusData.lastMachineState){
    return;
  }

  // updates last state
  StatusData.lastMachineState = StatusData.machineState;
  
  // TODO no action on change state, only display
  if(StatusData.machineState == STATE_IDLE){
    Sprintln("Idle state");
  }
  else if(StatusData.machineState == STATE_READY){
    
    // stops everything, shouldn't be necessary, but you never know
    StopRx();
    StopTx();

    Sprintln("Ready state");
  }
  else if(StatusData.machineState == STATE_RX_ON){
    // StartRx();
    Sprintln("RX is on");
  }
  else if(StatusData.machineState == STATE_RX_IN){
    Sprintln("RX is incoming");
  }
  else if(StatusData.machineState == STATE_TX_ON){
    // transmission prepared, next cicle will start transmitting
    Sprintln("TX is on");
    StatusData.machineState = STATE_TX_OUT;
  
  }
  else if(StatusData.machineState == STATE_TX_OUT){
    // once here the transmission begin
    readyForTx = true;
    Sprintln("TX is outgoing");
  }
  else if(StatusData.machineState == STATE_FATAL_ERROR){
    Sprintln("Fatal error");
    // TODO implement error print
  }

}


$(document).ready(function(){

  RenderReadyState();
  	//start manage loader
	$(document).ajaxSend(function(event, jqxhr, settings) {
    //default loader used
    if(settings.dataSkiploader){
      return;
    }
		$('#generalLoader').fadeIn(150);
	});

	$(document).ajaxComplete(function(event, jqxhr, settings) {
    $('#generalLoader').fadeOut(300);			
  });
  
  //end manage loader
  
  setInterval(function(){
    GetStatus(true, true)
  }, 500);

});

var Paths = {
  osConfig: '/osConfig',
  osSystem: '/osSystem',
  osAction: '/osAction',
  osStatus: '/osStatus'
}

var skipStatusReload = false;

function pad(num, size) {
  var s = "000000000" + num;
  return s.substr(s.length-size);
}

function ConvertMachineState(status){

  switch (status) {
    case 0:
      return 'STATE_IDLE';      
      break;
    case 1:
      return 'STATE_READY';      
      break;
    case 2:
      return 'STATE_RX_ON';      
      break;
    case 3:
      return 'STATE_RX_IN';      
      break;
    case 4:
      return 'STATE_TX_ON';      
      break;
    case 5:
      return 'STATE_TX_OUT';      
      break;

    default:
      return null;
    break;
  }

}

function osSystem(action) {

  var dataJSON = {
    action: action
  }

  $.ajax({
    type: "POST",
    url: Paths.osSystem,
    data: JSON.stringify(dataJSON),
    dataType: "json",
    success: function(msg) {

      location.reload();
    
    },
    error: function(jqXHR,error, errorThrown) { 
      alert("Operation failed"); 
      console.log("Something went wrong");
      
    }
  });

}

var lastState = 'STATE_IDLE';
function GetStatus(skipLoader) {

  if(skipStatusReload){
    return;
  }

  var dataJSON = {
    action: 'GetStatus'
  }

  $.ajax({
    type: "POST",
    url: Paths.osStatus,
    data: JSON.stringify(dataJSON),
    dataType: "json",
    dataSkiploader: skipLoader,
    success: function(msg) {
     
      RenderStatus(msg.response);
    
    },
    error: function(jqXHR,error, errorThrown) {  
      console.log("Something went wrong");
      
    }
  });
}

function StartRx() {

  skipStatusReload = true;

  var dataJSON = {
    action: 'startRx'
  }

  $.ajax({
    type: "POST",
    url: Paths.osAction,
    data: JSON.stringify(dataJSON),
    dataType: "json",
    dataSkiploader: false,
    success: function(msg) {
      
      // RenderStatus(msg.response);
      
      skipStatusReload = false;
    },
    error: function(jqXHR,error, errorThrown) {  
      console.log("Something went wrong");
      
    }
  });
}

function StopRx() {

  skipStatusReload = true;

  var dataJSON = {
    action: 'stopRx'
  }

  $.ajax({
    type: "POST",
    url: Paths.osAction,
    data: JSON.stringify(dataJSON),
    dataType: "json",
    dataSkiploader: false,
    success: function(msg) {
      
      // RenderStatus(msg.response);
      
      skipStatusReload = false;
    },
    error: function(jqXHR,error, errorThrown) {  
      console.log("Something went wrong");
      
    }
  });
}

function SendTxData(){

  // let txMsg ={
  //   code: 3696136,
  //   protocol: 1,
  //   pulseLength: 313,
  //   repeat: 1,
  //   length: 24
  // }
   
  let txMsg ={
    code: $('#transmitt_decimal').val(),
    protocol: $('#transmitt_protocol').val(),
    pulseLength: $('#transmitt_pulseLength').val(),
    repeat: 1,
    length: $('#transmitt_length').val()
  }

  StartTx(txMsg);
}

function SendTimedTxData(){

  let txMsg ={
    code: 3696136,
    protocol: 1,
    pulseLength: 313,
    repeat: 1,
    length: 24
  }


  StartTx(txMsg);
}

function StartReady() {

  skipStatusReload = true;

  var dataJSON = {
    action: 'startReady'
  }

  $.ajax({
    type: "POST",
    url: Paths.osAction,
    data: JSON.stringify(dataJSON),
    dataType: "json",
    dataSkiploader: false,
    success: function(msg) {
      
      // RenderStatus(msg.response);
      
      skipStatusReload = false;
    },
    error: function(jqXHR,error, errorThrown) {  
      console.log("Something went wrong");
      
    }
  });
}

function StartTx(txMsg) {

  skipStatusReload = true;

  var dataJSON = {
    action: 'sendTx',
    msg: txMsg
  }

  $.ajax({
    type: "POST",
    url: Paths.osAction,
    data: JSON.stringify(dataJSON),
    dataType: "json",
    dataSkiploader: false,
    success: function(msg) {
      console.log(msg);
      
      // RenderStatus(msg.response);
      
      skipStatusReload = false;
    },
    error: function(jqXHR,error, errorThrown) {  
      console.log("Something went wrong");
      
    }
  });
}

function StopTx() {

  skipStatusReload = true;

  var dataJSON = {
    action: 'stopTx'
  }

  $.ajax({
    type: "POST",
    url: Paths.osAction,
    data: JSON.stringify(dataJSON),
    dataType: "json",
    dataSkiploader: false,
    success: function(msg) {
      console.log(msg);
      
      
      skipStatusReload = false;
    },
    error: function(jqXHR,error, errorThrown) {  
      console.log("Something went wrong");
      
    }
  });
}

function RenderStatus(statusData){
  
  // makes the state human redable
  let currentState = ConvertMachineState(statusData.state);

  // no changes
  if(lastState == currentState){
    return;
  }
  // console.log('machineState:' + ConvertMachineState(statusData.state));
  // console.log(statusData);

  // 'STATE_READY';      
  if(currentState == 'STATE_READY'){
    
    // rx completed
    if(lastState == 'STATE_RX_IN' && statusData.messages.length > 0){
      RenderRecievedData(statusData);
    }

    RenderReadyState(statusData);
        
  }

  if(currentState == 'STATE_RX_ON'){
    RenderRxOnState(statusData);
  }

  if(currentState == 'STATE_RX_IN'){
    RenderRxInState(statusData);
  }

  if(currentState == 'STATE_TX_ON'){
    RenderTxOnState(statusData);
  }

  if(currentState == 'STATE_TX_OUT'){
    RenderTxOutState(statusData);
  }

  
  // updates last status
  lastState = currentState;

}

function ShowPills(pills){
  $(pills).show();
}

function HidePills(pills){
  $(pills).hide();
}


function RenderReadyState(statusData){
  console.log('Ready state');

  // buttons
  $('.stopBtn').prop("disabled", true);
  $('.startBtn').prop("disabled", false);

  ShowPills('.statusPillOff');
  $('.statusPill:not(.statusPillOff, .recieve_statusPillNewData)').hide();
  
}

function RenderRxOnState(statusData){
  
  ShowPills('.recieve_statusPillReady');
  HidePills('.recieve_statusPillOff, .recieve_statusPillRecieving, .recieve_statusPillNewData');
  
  // startBtn stopBtn

  // disable recieve start button
  $("#divRecieve .startBtn").prop("disabled", true);

  // enable recieve stop button
  $("#divRecieve .stopBtn").prop("disabled", false);


  // disable transmitt start and stop button
  $("#divTransmitt .startBtn, #divTransmitt .stopBtn").prop("disabled", true);

  $("#transmitt_btnCopyFromRecieve").prop("disabled", true);

  console.log('Rx ON');

}

function RenderRxInState(statusData){

  ShowPills('.recieve_statusPillRecieving');
  HidePills('.recieve_statusPillOff, .recieve_statusPillReady');
  
  console.log('Rx IN');

}

function RenderRecievedData(recievedData){
  
  ShowPills('.recieve_statusPillNewData');
  $("#transmitt_btnCopyFromRecieve").prop("disabled", false);
  
  // clear old values
  $('.recievedData').val('');


  var recievedPackets = recievedData.messages.length;
  var midPl = 0;
  var minPl = 9999;
  var maxPl = 0;

  var packet;
  for (let i = 0; i < recievedPackets; i++) {
    packet = recievedData.messages[i];

    // check minimum
    if(packet.pulseLength <= minPl){
      minPl = packet.pulseLength;
    }

    // check maximus
    if(packet.pulseLength >= maxPl){
      maxPl = packet.pulseLength;
    }

    // sum to middle
    midPl += packet.pulseLength;
  }

  // final mid calculation
  midPl = midPl/recievedPackets;

  // get common data from last packet
  $('#recieveFormatted_decimal').val(packet.decimal);
  $('#recieveFormatted_binary').val(packet.binary);
  $('#recieveFormatted_pulseLengthMin').val(minPl);
  $('#recieveFormatted_pulseLengthMax').val(maxPl);
  $('#recieveFormatted_pulseLengthMid').val(Math.round(midPl));
  $('#recieveFormatted_packetsCaptured').val(recievedPackets);
  $('#recieveFormatted_length').val(packet.length);
  $('#recieveFormatted_protocol').val(packet.protocol);
}

function RenderTxOnState(){

  ShowPills('.transmitt_statusPillReady');
  HidePills('.transmitt_statusPillOff, .transmitt_statusPillSending');
  
  // startBtn stopBtn

}

function RenderTxOutState(){
  ShowPills('.transmitt_statusPillSending');
  HidePills('.transmitt_statusPillOff, .transmitt_statusPillReady');
  
  // enable transmitt stop
  $("#divTransmitt .stopBtn").prop("disabled", false);

  // disable transmitt start button
  $("#divTransmitt .startBtn").prop("disabled", true);
      
  // disable recieve start and stop button
  $("#divRecieve .startBtn, #divRecieve .stopBtn").prop("disabled", true);

  // $("#transmitt_btnCopyFromRecieve, #transmitt_btnClear").prop("disabled", true);

  console.log('Tx OUT');

}

function CopyRxData(){
  ClearTxData();

  $('#transmitt_decimal').val( $('#recieveFormatted_decimal').val() );
  $('#transmitt_binary').val( $('#recieveFormatted_binary').val() );
  $('#transmitt_pulseLength').val( Math.round($('#recieveFormatted_pulseLengthMid').val()) );
  $('#transmitt_protocol').val( $('#recieveFormatted_protocol').val() );
  $('#transmitt_length').val( $('#recieveFormatted_length').val() );

}

function ClearTxData(){
  $('.transmitData').val('');
}